<?php

    $x = 20;
    $y = 30;

    function test1() {
        global $x, $y;
        $x = $y + $x;
    }

    test1();

    echo $x;

    echo "<p>globals</p>";
    echo $GLOBALS['x'] . " " . $GLOBALS['y'] . " ";
    echo var_dump($GLOBALS);