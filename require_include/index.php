<p>
    <em>require</em> - bisa buat ditambah ke file,
    jika tidak ada file, maka semua file di bawah kode tsb akan hilang.<br /><br />

    <em>include</em> - berbeda dengan <em>require</em>, hanya file 
    yang tidak ada akan hilang, tidak diikuti dengan kode di bawahnya.
</p>

<?php

include "header_include.php";      // hilang
    include "header.php";          // ada

    echo "hello from index.php";   // ada

    require "footer_include.php";  // hilang
    require "footer.php";          // hilang walaupun
                                   //   filenya ada

    echo "<p>di bawah footer</p>"; // hilang

?>