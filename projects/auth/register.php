<?php include "includes/header.php"; ?>

<?php require "config.php"; ?>

<?php

    if (isset($_POST["register"])) {
        if ($_POST["email"] == "" OR
            $_POST["password"] == "") {
            echo "some inputs are empty";
        } else {
            // parameter value
            $email = $_POST["email"];
            $password = $_POST["password"];

            // insert dan execute query
            $insert = $conn->prepare(
                "INSERT INTO user (email, password) VALUES(:email, :mypassword)"
            );
            $insert->execute([
                ":email" => $email,
                ":mypassword" => password_hash($password, PASSWORD_DEFAULT,),
            ]);

        }
    }

?>

    <main>
        <form action="register.php" method="post">
            <input type="email" name="email" id="email" placeholder="masukan email" />
            <br />

            <input type="password" name="password" id="password" placeholder="masukan password" />
            <br />

            <input type="submit" name="register" value="register" />
        </form>
    </main>

<?php include "includes/footer.php"; ?>
