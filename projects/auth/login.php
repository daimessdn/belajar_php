<?php include "includes/header.php"; ?>

<?php require "config.php"; ?>

<?php

    if (isset($_POST["login"])) {
        if ($_POST["email"] == "" OR
            $_POST["password"] == "") {
            echo "some inputs are empty";
        } else {
            // parameter value
            $email = $_POST["email"];
            $password = $_POST["password"];

            // prepare dan execute query
            $login = $conn->prepare(
                "SELECT * FROM user where email = '$email'"
            );
            $login->execute();

            // get data dari db bds query
            $data = $login->fetch(PDO::FETCH_ASSOC);

            if ($login->rowCount() > 0) {
                // echo $login->rowCount();

                // echo $data["password"];

                if (password_verify($password, $data["password"])) {
                    $_SESSION["email"] = $data["email"];

                    header("location: index.php");

                    // echo "logged in";
                } else {
                    echo "email or password is incorrect";
                }
            } else {
                echo "email or password is incorrect";
            }

        }
    }

?>

    <main>
        <form action="login.php" method="post">
            <input type="email" name="email" id="email" placeholder="masukan email" />
            <br />

            <input type="password" name="password" id="password" placeholder="masukan password" />
            <br />

            <input type="submit" name="login" value="login" />
        </form>
    </main>

<?php include "includes/footer.php"; ?>
