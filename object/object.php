<?php

    class Person {
        public $fname;
        public $lname;
        public $age;

        public function __construct($fname, $lname, $age)
        {
            $this->fname = $fname;
            $this->lname = $lname;
            $this->age = $age;
        }

        public function hello() {
            return "I'm " . $this->fname .
                " " . $this->lname . ", my age is " .
                $this->age . " years old.";
        }
    }

    // init object/class
    $person1 = new Person("bambang", "aja", 22);
    $person2 = new Person("sarah", "aja", 21);

    // panggil hello
    echo $person1->hello() . "<br><br>";
    echo $person2->hello();