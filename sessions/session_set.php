<?php

    // start/memulai PHP session
    session_start();

    // update nilai session
    $_SESSION["username"] = "bambang";
    $_SESSION["age"] = 26;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php session</title>
</head>
<body>
    <h3><?php 
        echo $_SESSION["username"] . " is " . $_SESSION['age'] . " years old";
    ?></h3>

    <p>session updated</p>
</body>
</html>