<?php

    // start/memulai PHP session
    session_start();

    // menghapus semua variable session
    session_unset();

    // keluar dari session
    session_destroy();
?>

<p>session destroyed</p>