<?php ?>

<h1>Format tanggal</h1>

<p>
<ul>
    <li><strong>d</strong> - day of the month (01 - 31)
    </li>

    <li><strong>m</strong> - month (01 - 12)</li>

    <li><strong>Y</strong> - four digit year</li>

    <li><strong>l (lowercase L)</strong> - day of the week</li>
</ul>
</p>

<p>
    <?php

    echo "l, d-m-Y: " . date("l, d-m-Y");

    ?>
</p>

<hr />

<h1>format waktu</h2>

    <p>
    <ul>
        <li><strong>H</strong> - jam format 24 jam</li>
        <li><strong>h</strong> - jam format 12 jam</li>
        <li><strong>i</strong> - jumlah menit (00 - 59)</li>
        <li><strong>s</strong> - jumlah detik (00 - 59)</li>
        <li><strong>a</strong> - am atawa pm</li>
    </ul>

    <?php

    echo "dapetin <strong>timezone date_default_timezone_get();<strong><br />";
    echo date_default_timezone_get() . "<br />";

    ?>
    </p>

    <p>
        <?php

        echo "dapetin <strong>timezone date_default_timezone_set('Asia/Jakarta');<strong><br />";
        echo date_default_timezone_set('Asia/Jakarta') . "<br /><br />";
        echo date_default_timezone_get() . "<br />";

        ?>
    </p>

    <p>
        Contoh waktu ("H:i:s"):<br />

        <?php

        echo date("H:i:s");

        ?>
    </p>

    <p>
        Contoh waktu ("h:i:s a"):<br />

        <?php

        echo date("h:i:s A");

        ?>
    </p>